#!/usr/bin/env python3

import pyais
from pyais.messages import AISSentence
from rclpy.node import Node
from aismsg.msg import AISObjectStamped, AISObjectArray
from ais2msg.object_helper import get_AISobj_from_decode_msg, decode_message

QUEUE_SIZE = 1000
OUTDATED_THRESHOLD_SEC = 20
OUTDATED_THRESHOLD_NANOSEC = OUTDATED_THRESHOLD_SEC * 1000000000
AIS_OBJECT_PUBLISHER_NAME = "AIS_Publisher"
AIS_OBJECT_PUBLISHER_PUB_TOPIC = "/AISObjectPub"
AIS_OBJECT_ARRAY_PUBLISHER_NAME = "AIS_Array_Publisher"
AIS_OBJECT_ARRAY_PUBLISHER_PUB_TOPIC = "/AISObjectArrayPub"


class AISObjectPublisher(Node):

    def __init__(self, nodename=None):
        if nodename is None:
            super().__init__(AIS_OBJECT_PUBLISHER_NAME)
        else:
            super().__init__(nodename)

        self._ros_setup()

        self.outdated_threshold_nanosec = OUTDATED_THRESHOLD_NANOSEC

        self.counter = 0
        self.last_mmsi = None
        self.last_timestamp = None

        self.mmsi_obj_dict = {}

    def publish_data(self, data):

        try:
            decode_msg = decode_message(data)
        except RuntimeError:
            self.get_logger().warning(
                f"Got Invalid Message Type {type(data)}!")
            return

        self.last_mmsi = str(decode_msg.mmsi)
        try:
            if self.last_mmsi in self.mmsi_obj_dict:
                # if mmsi exist, we do update
                aisobj = get_AISobj_from_decode_msg(
                    decode_msg, self.mmsi_obj_dict[self.last_mmsi])
                obj = aisobj.object
                self.get_logger().info(f"Update Exist mmsi: {self.last_mmsi}")
            else:
                # if mmsi not exist, create a new one

                aisobj = get_AISobj_from_decode_msg(decode_msg)
                obj = aisobj.object
                obj.timestamp_create = self.get_clock().now().nanoseconds
                self.mmsi_obj_dict[self.last_mmsi] = aisobj
                self.get_logger().info(f"Got New mmsi: {self.last_mmsi}")

            aisobj.object.timestamp_last_update = self.get_clock().now(
            ).nanoseconds
            aisobj.header.stamp = self.get_clock().now().to_msg()
            # aisobj.header.frame_id = "base_link"
            self.counter += 1
            self.last_timestamp = aisobj.object.timestamp_last_update

        except RuntimeError:
            self.get_logger().warning(
                f"Got Unsupported Message Type {int(decode_msg.msg_type)}!")
            return

        self._publish()
        self._remove_outdated()

    def _ros_setup(self):
        self.publisher = self.create_publisher(AISObjectStamped,
                                               AIS_OBJECT_PUBLISHER_PUB_TOPIC,
                                               QUEUE_SIZE)

    def _publish(self):
        self.publisher.publish(self.mmsi_obj_dict[self.last_mmsi])

    def _remove_outdated(self):

        out_mmsi = [
            k for k, v in self.mmsi_obj_dict.items() if self.last_timestamp -
            v.object.timestamp_last_update > self.outdated_threshold_nanosec
        ]

        for mmsi in out_mmsi:
            self.mmsi_obj_dict.pop(mmsi)
            self.get_logger().info(f"Remove outdated mmsi: {mmsi}")


class AISObjectArrayPublisher(AISObjectPublisher):

    def __init__(self):
        super().__init__(AIS_OBJECT_ARRAY_PUBLISHER_NAME)

    def _ros_setup(self):
        self.publisher = self.create_publisher(
            AISObjectArray, AIS_OBJECT_ARRAY_PUBLISHER_PUB_TOPIC, QUEUE_SIZE)

    def _publish(self):
        arr = AISObjectArray()
        arr.array = [v for _, v in self.mmsi_obj_dict.items()]
        self.publisher.publish(arr)
