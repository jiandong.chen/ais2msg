import pyais
from pyais.messages import AISSentence
from aismsg.msg import AISObjectStamped
from ais2msg.util import process_message_data


def decode_message(data):
    if isinstance(data, str):
        decode_msg = pyais.decode(data)
    elif isinstance(data, (list, tuple)):
        # if data consist of multi sentences
        decode_msg = pyais.decode(*data)
    elif isinstance(data, AISSentence):
        # for tcp case, get AISSentece
        decode_msg = data.decode()
    else:
        raise RuntimeError(f"Got Invalid Message Type {type(data)}!")

    return decode_msg


def get_AISobj_from_decode_msg(decode_msg, exist_obj=None):

    if exist_obj is None:
        aisobj = AISObjectStamped()
    else:
        aisobj = exist_obj

    obj = aisobj.object
    msg_type = process_data(decode_msg, obj)
    obj.mmsi = get_object_data_from_type(msg_type, obj).ais_header.mmsi
    obj.last_update_msg_type = msg_type

    return aisobj


def process_data(decode_msg, obj):
    msg_type = int(decode_msg.asdict()["msg_type"])

    if msg_type not in (1, 2, 3, 4, 5, 8):
        raise RuntimeError("This Msg Type is NOT supported")

    set_type_indicator(obj, msg_type)

    process_message_data(decode_msg, get_object_data_from_type(msg_type, obj))

    return msg_type


def get_object_data_from_type(msg_type, obj):

    msg_type = int(msg_type)
    if msg_type in (1, 2, 3):
        return obj.position
    elif msg_type == 4:
        return obj.base_station
    elif msg_type == 5:
        return obj.static_voyage
    elif msg_type == 8:
        return obj.bin_broadcast
    else:
        return None


def is_valid_type(type):

    return type > 0 and type <= 27


def set_type_indicator(obj, type):

    if not is_valid_type(type):
        return False

    obj.available_msg_type = obj.available_msg_type | (1 << (type - 1))

    return True


def is_message_available(obj, type):

    if not is_valid_type(type):
        return False

    return obj.available_msg_type != 0 and obj.available_msg_type & (
        1 << (type - 1))
