def process_message_data(decode_msg, msg):

    decode_msg_dict = decode_msg.asdict()
    process_header(decode_msg_dict, msg.ais_header)

    for key, value in decode_msg_dict.items():
        process_field_data(key, value, msg)

    if msg.ais_header.msg_type in (1, 2, 3, 4, 9, 11, 18):
        process_radio(decode_msg, msg.radio)


def process_header(decode_msg_dict, header):

    header.msg_type = decode_msg_dict["msg_type"]
    header.repeat = decode_msg_dict["repeat"]
    header.mmsi = str(decode_msg_dict["mmsi"])


def process_field_data(key, value, msg):

    if key in ("msg_type", "repeat", "mmsi", "radio", "spare_1"):
        return

    if key == "status":
        msg.status = int(value)
    elif key == "turn":
        msg.turn = float(value)
    elif key == "speed":
        msg.speed = value
    elif key == "accuracy":
        msg.accuracy = value
    elif key == "lon":
        msg.lon = value
    elif key == "lat":
        msg.lat = value
    elif key == "course":
        msg.course = value
    elif key == "heading":
        msg.heading = value
    elif key == "second":
        msg.second = value
    elif key == "maneuver":
        msg.maneuver = int(value)
    elif key == "raim":
        msg.raim = value
    elif key == "year":
        msg.year = value
    elif key == "month":
        msg.month = value
    elif key == "day":
        msg.day = value
    elif key == "hour":
        msg.hour = value
    elif key == "minute":
        msg.minute = value
    elif key == "second":
        msg.second = value
    elif key == "epfd":
        msg.epfd = int(value)
    elif key == "ais_version":
        msg.ais_version = value
    elif key == "imo":
        msg.imo = value
    elif key == "callsign":
        msg.callsign = str(value)
    elif key == "shipname":
        msg.shipname = str(value)
    elif key == "ship_type":
        msg.ship_type = int(value)
    elif key == "to_bow":
        msg.to_bow = value
    elif key == "to_stern":
        msg.to_stern = value
    elif key == "to_port":
        msg.to_port = value
    elif key == "to_starboard":
        msg.to_starboard = value
    elif key == "draught":
        msg.draught = value
    elif key == "destination":
        msg.destination = str(value)
    elif key == "dte":
        msg.dte = value
    elif key == "dac":
        msg.dac = value
    elif key == "fid":
        msg.fid = value
    elif key == "data":
        msg.data = list(value)
    else:
        print(f"Unspecified data type \"{key}\" given! Ignore!!")


def process_radio(decode_msg, radio):

    radio.status = decode_msg.asdict()["radio"]
    radio.is_sotdma = decode_msg.is_sotdma
    communication_state = decode_msg.get_communication_state()

    for key, value in communication_state.items():

        if value == None:
            continue

        process_radio_field_data(key, value, radio)


def process_radio_field_data(key, value, ros_radio_msg):

    if key == "sync_state":
        ros_radio_msg.sync_state = int(value)
    elif key == "received_stations":
        ros_radio_msg.receive_station_num = value
    elif key == "slot_timeout":
        ros_radio_msg.slot_timeout = value
    elif key == "slot_number":
        ros_radio_msg.slot_num = value
    elif key == "slot_offset":
        ros_radio_msg.slot_offset = value
    elif key == "utc_hour":
        ros_radio_msg.utc_hour = value
    elif key == "utc_minute":
        ros_radio_msg.utc_minute = value
    elif key == "keep_flag":
        ros_radio_msg.keep_flag = value
    elif key == "slot_increment":
        ros_radio_msg.slot_increment = value
    elif key == "num_slots":
        ros_radio_msg.allocate_slot = value
    else:
        print(f"Unspecified radio data type \"{key}\" given! Ignore!!")

