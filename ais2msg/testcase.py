TESTCASE = [
    "!AIVDM,1,1,,A,15RTgt0PAso;90TKcjM8h6g208CQ,0*4A",  # type 1
    "!AIVDM,1,1,,B,25Cjtd0Oj;Jp7ilG7=UkKBoB0<06,0*60",  # type 2
    "!AIVDM,1,1,,A,38Id705000rRVJhE7cl9n;160000,0*40",  # type 3
    "!AIVDM,1,1,,A,403OviQuMGCqWrRO9>E6fE700@GO,0*4D",  # type 4
    [
        "!AIVDM,2,1,1,A,55?MbV02;H;s<HtKR20EHE:0@T4@Dn2222222216L961O5Gf0NSQEp6ClRp8,0*1C",
        "!AIVDM,2,2,1,A,88888888880,2*25"
    ],  # type 5
    "!AIVDM,1,1,,B,6B?n;be:cbapalgc;i6?Ow4,2*4A",
    "!AIVDO,1,1,4,B,8>jR06@0Bk3:vOli;L`nwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwt1,2*0E",  # type 8
]
