#!/usr/bin/env python3

import pyais
import rclpy
from itertools import cycle
from time import sleep
from pyais.stream import TCPConnection
from ais2msg.object_helper import get_AISobj_from_decode_msg
from ais2msg.testcase import TESTCASE
from ais2msg.aismsg_publisher import AISObjectPublisher, AISObjectArrayPublisher

HOST = '192.168.31.157'
PORT = 10009


def tcp_demo():

    rclpy.init()
    node = AISObjectPublisher()

    for data in TCPConnection(HOST, port=PORT):

        node.publish_data(data)


def arr_node_demo():
    rclpy.init()
    node = AISObjectArrayPublisher()
    pool = cycle(TESTCASE)

    for t in pool:
        node.publish_data(t)
        sleep(1)


def obj_node_demo():
    rclpy.init()
    node = AISObjectPublisher()
    pool = cycle(TESTCASE)

    for t in pool:
        node.publish_data(t)
        sleep(1)


def static_demo():

    for idx, msg in enumerate(TESTCASE):

        if isinstance(msg, str):
            decode_msg = pyais.decode(msg)
        else:
            decode_msg = pyais.decode(*msg)

        obj = get_AISobj_from_decode_msg(decode_msg)
        print("*" * 30 + " idx " + str(idx + 1) + " " + "*" * 30)
        print(obj)


if __name__ == '__main__':
    # static_demo()
    # obj_node_demo()
    arr_node_demo()
    # tcp_demo()
